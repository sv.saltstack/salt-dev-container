#!/usr/bin/env bash
set -euo pipefail

source /saltenv/bin/activate

cp /salt/conf/master /salt/conf/minion /saltenv/etc/salt

pip install -e /salt

cd /saltenv

DEBUGPY="python -X frozen_modules=off -m debugpy --listen 0.0.0.0:5678"
if [[ $WAIT_FOR_CLIENT -ne 0 ]]; then
  DEBUGPY="${DEBUGPY} --wait-for-client"
fi

if [[ $DEBUG_MASTER -ne 0 ]]; then
  MINION_DEBUG=""
  MASTER_DEBUG="${DEBUGPY}"
  LOG="master"
else
  MINION_DEBUG="${DEBUGPY}"
  MASTER_DEBUG=""
  LOG="minion"
fi

echo "Starting Master"
[[ $DEBUG_MASTER -ne 0 ]] && [[ $WAIT_FOR_CLIENT -ne 0 ]] && echo "Waiting for client connection..."
master="$(command -v salt-master)"
${MASTER_DEBUG} "${master}" -c ./etc/salt -l debug -d
echo "Master running"
sleep 5
echo "Starting Minion"
[[ $DEBUG_MASTER -ne 1 ]] && [[ $WAIT_FOR_CLIENT -ne 0 ]] && echo "Waiting for client connection..."
minion="$(command -v salt-minion)"
${MINION_DEBUG} "${minion}" -c ./etc/salt -l debug -d
echo "Minion running"

tail -f ./var/log/salt/${LOG}
