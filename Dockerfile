FROM debian:bookworm-slim

ENV DEBUG_MASTER=0
ENV WAIT_FOR_CLIENT=0

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y\
    && apt-get install -y --no-install-recommends sed python-is-python3 python3-venv python3-pip python3-dev build-essential gdb \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /saltenv/etc/salt/pki/{master,minion} /saltenv/var/run

WORKDIR /saltenv
RUN python -m venv /saltenv \
    && . /saltenv/bin/activate \
    && pip install pyzmq PyYAML pycrypto msgpack jinja2 psutil futures tornado pylint saltpylint debugpy Sphinx==1.3.1

COPY ./etc/salt /saltenv/etc/salt

COPY ./entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh"]

LABEL org.opencontainers.image.authors="Mark Bools"
LABEL org.opencontainers.image.url="https://gitlab.com/sv.saltstack/salt-dev-container"
LABEL org.opencontainers.image.documentation="https://gitlab.com/sv.saltstack/salt-dev-container"
LABEL org.opencontainers.image.source="https://gitlab.com/sv.saltstack/salt-dev-container"
LABEL org.opencontainers.image.vendor="SaltyVagrant"
LABEL org.opencontainers.image.licenses="MIT"
