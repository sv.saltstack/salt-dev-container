# Salt Development Container

A simple Docker container to run Salt in a development environment with `debugpy`.

## Usage

Basic usage, clone the salt repository and run the container mounting the salt repository to `/salt` in the container.

```bash
git clone https://github.com/saltstack/salt.git
docker run -v "$(pwd)/salt:/salt" -p 5678:5678 -it saltyvagrant/salt-dev
```

This starts a Salt master and minion in the container and attaches `debugpy` to the minion, making it available to the host on port `5678`.

If you need to debug the minion startup, set the environment variable `WAIT_FOR_CLIENT` to `1`. The `debugpy` server will then wait for your client to attach before running the minion.

```bash
docker run -e WAIT_FOR_CLIENT=1 -v "$(pwd)/salt:/salt" -p 5678:5678 -it saltyvagrant/salt-dev
```

If you need to debug the Salt master, set the environment variable `DEBUG_MASTER` to `1`. The `debugpy` server will be attached to the Salt master rather that the Salt minion.

Salt is installed in edit mode, so changes made will be reflected in the container runtime.

The log for the minion or master will be `tail`ed to `stdout` according to which one is being debugged.

Checked against `salt 3007.0` sources.

## Helper scripts

Three simple wrappers to inject common `salt` commands into the container.

- `salt` e.g. `./scripts/salt '*' test.version`
- `salt-call` e.g. `./scripts/salt-call test.version`
- `salt` e.g. `./scripts/salt-run jobs.list_jobs`
- `start`---start a salt-dev container
- `stop`---stop a running salt-dev container

These scripts use environment variable `SALT_CONTAINER` to determine where commands are executed. If not set the scripts will try to find a container derived from `saltyvagrant/salt-dev` and use that.

### The `start`/`stop` scripts

These provide a convenient way to start and stop a salt-dev container.

By default these scripts expect your project to look loke this:

```
proj/
  salt/       # The salt repository
  salt-dev/   # This repository
  srv/        # Any test state/pillar/formula
    salt/
    pillar/
    formulas/
```

You can point to a different salt repository directory by setting `SALT_REPO`.

The `start` script returns the id of the docker started. this can be assigned to `SALT_CONTAINER` and will then be used by other script. If `SALT_CONTAINER` is not set the scripts try to find a container derived from `saltyvagrant/salt-dev` and use that.
